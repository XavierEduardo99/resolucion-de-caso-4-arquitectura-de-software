
# Resolución de caso 4.1 - Arquitectura de Software

### Requisitos previos

Si es que no se tiene instalado PHP 7.4, PostgreSQL, Composer y Laravel 8, use los siguientes enlaces:

 - Windows:
    - [XAMPP](https://sourceforge.net/projects/xampp/files/XAMPP%20Windows/7.4.33/xampp-windows-x64-7.4.33-0-VC15-installer.exe/download)
    - [Guía de instalación de Laravel en Windows](https://laravel.com/docs/8.x/installation#your-first-laravel-project)

 - Linux:
    - [Guía de instalación de LAMP en Debian 12](https://voidnull.es/instalacion-de-servidor-web-lamp-en-debian-12/)
    - [Guía de instalación de LAMP en Fedora 35](https://computingforgeeks.com/how-to-install-lamp-stack-on-fedora/)
    - [Guía de instalación de LAMP en Arch Linux](https://www.tecmint.com/install-lamp-in-arch-linux/)
    - [Guía de instalación de Laravel en Linux](https://laravel.com/docs/8.x/installation#your-first-laravel-project)

Además, necesitará contar con [Composer](https://getcomposer.org/download/)
Para instalarlo en Microsoft Windows, solamente utilice el wizzard de instalación.
Para instalarlo en una distribución GNU Linux, realice lo siguiente:
```sh
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"
$ sudo mv composer.phar /usr/local/bin/composer
```

## Instalación

Una vez haya clonado el repostorio Git, deberá ir a la carpeta raíz del proyecto y realizar lo siguiente:
 - Configure el archivo `example.env`, con el nombre del proyecto, esquema y la contraseña necesarios para conectar a su base de datos.
 - Renombre el archivo `example.env` a `.env` (Si se encuentra en un computador con Linux o macOS, es probable que este archivo se oculte).

Abra un Terminal o Símbolo del sistema y realice lo siguiente:
 - Dentro del terminal, ejecute: `composer install` o `composer update`
 - Dentro del terminal, ejecute: `php artisan key:generate`
 - Dentro del terminal, ejecute: `composer dump-autoload`

 - Dentro del terminal, ejecute: `php artisan migrate`
 - Dentro del terminal, ejecute: `php artisan storage:link`

Para actualizar el caché realice lo siguiente:
 - Dentro de un terminal, ejecute: `php artisan cache:clear`
 - Dentro de un terminal, ejecute: `php artisan config:cache`

Una vez instalado todo se manda a ejecutar la página
- Dentro de un terminal, ejecute: `php artisan serve`

Tome en cuenta que para ejecutar esta aplicación deberá contar con las siguientes extensiones de PHP 7.4:
- `php7.4`
- `php7.4-cli`
- `php7.4-curl`
- `php7.4-mbstring`
- `php7.4-mysql` (En caso de que se vaya a utilizar MySQL o MariaDB)
- `php7.4-pgsql` (En caso de que se vaya a utilizar PostgreSQL)
- `php7.4-opcache`
- `php7.4-readline`
- `php7.4-xml`
- `php7.4-zip`
