<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::group(['middleware' => ['auth']], function() {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('products', App\Http\Controllers\ProductController::class);
    Route::resource('shoppingCart', App\Http\Controllers\ShoppingCartController::class);

    Route::get('/showCheckout', 'App\Http\Controllers\ShoppingCartController@showCheckout');

});

Auth::routes();