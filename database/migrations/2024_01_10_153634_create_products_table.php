<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('titulo')->nullable();
            $table->text('descripcion')->nullable();
            $table->float('precio')->nullable();
            $table->float('porcentajeDescuento')->nullable();
            $table->float('raiting')->nullable();
            $table->unsignedInteger('stock')->nullable();
            $table->string('marca')->nullable();
            $table->text('miniatura')->nullable()->default('https://upload.wikimedia.org/wikipedia/commons/b/b1/Missing-image-232x150.png');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
