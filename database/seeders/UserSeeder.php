<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador VentaVirtual',
            'email' => 'admin@ventavirtual.com',
            'password' => Hash::make('admin'),
            'role_id' => 1
        ]);

        DB::table('users')->insert([
            'name' => 'Vendedor VentaVirtual',
            'email' => 'vendedor@ventavirtual.com',
            'password' => Hash::make('admin'),
            'role_id' => 2
        ]);

        DB::table('users')->insert([
            'name' => 'Cliente VentaVirtual',
            'email' => 'cliente@ventavirtual.com',
            'password' => Hash::make('admin'),
            'role_id' => 3
        ]);

        DB::table('users')->insert([
            'name' => 'Auditor Externo',
            'email' => 'auditor@ventavirtual.com',
            'password' => Hash::make('admin'),
            'role_id' => 4
        ]);
    }
}
