<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scriptPath = public_path('sql/ProductsInsert.sql');
        $sqlStatement = file_get_contents($scriptPath);

        DB::unprepared($sqlStatement);
    }
}
