<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
            'description' => 'Este es el rol de administrador.',
        ]);

        DB::table('roles')->insert([
            'name' => 'Vendedor',
            'description' => 'Este es el rol de vendedor.',
        ]);

        DB::table('roles')->insert([
            'name' => 'Cliente',
            'description' => 'Este es el rol de cliente.',
        ]);

        DB::table('roles')->insert([
            'name' => 'Auditor',
            'description' => 'Este es el rol de auditor.',
        ]);
    }
}
