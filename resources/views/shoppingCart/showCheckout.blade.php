@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4><i class="fa-solid fa-user-check"></i> Confirmación de compra</h4></div>

                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Número de tarjeta</label>
                                            <input type="text" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 2%;">
                                        <div class="col-md-12">
                                            <label>Nombre del tarjetahabiente</label>
                                            <input type="text" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 2%;">
                                        <div class="col-md-4">
                                            <label>Mes de caducidad</label>
                                            <select class="form-control" required>
                                                <option value="">Seleccione...</option>
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8">08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Año de caducidad</label>
                                            <select class="form-control" required>
                                                <option value="">Seleccione...</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>CCV</label>
                                            <input type="text" placeholder="123..." class="form-control">
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 2%; text-align: center">
                                        <div class="col-md-12">
                                            <input type="checkbox" id="invoiceCheck" checked value="invoice">
                                            <label for="invoiceCheck">Emitir factura con los datos de la cuenta.</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer" style="text-align: center;">
                    <h5>Total a cancelar: ${{ $totalAPagar }}</h5>
                    <button class="btn btn-primary">Confirmar pago</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
