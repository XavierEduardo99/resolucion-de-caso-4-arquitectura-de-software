@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4><i class="fa-solid fa-cart-shopping"></i> Su carrito de compras</h4></div>

                <div class="card-body">
                    <table class="table table-striped table-hover" id="mainTable">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Costo</th>
                                <th>Cantidad</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($shoppingCarts as $shoppingCart)
                                <tr>
                                    <td>{{ $shoppingCart->product->titulo }}</td>
                                    <td>${{ $shoppingCart->product->precio }}</td>
                                    <td>{{ $shoppingCart->cantidadProducto }}</td>

                                    <td>
                                        <form method="POST" action="{{ route('shoppingCart.destroy', $shoppingCart->id) }}">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" class="btn btn-sm btn-danger warning show_confirm"><i class="fa-solid fa-xmark"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                        <tfoot>
                            <tr>
                                <th>Producto</th>
                                <th>Costo</th>
                                <th>Cantidad</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="card-footer" style="text-align: right;">
                    <h5>Total a cancelar: ${{ $totalAPagar }}</h5>
                </div>

                <div style="text-align: center;">
                    @if (sizeof($shoppingCarts)>0)
                        <a class="btn btn-primary" href="{{ url('/showCheckout') }}"><i class="fa-solid fa-credit-card"></i> Proceder a pagar</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        new DataTable('#mainTable');
    });
</script>

@endsection
