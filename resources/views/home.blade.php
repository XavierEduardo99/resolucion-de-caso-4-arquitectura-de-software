@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Lo destacado de hoy:') }}</div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($products as $product)
                            <div class="col-md-4" style="padding-bottom: 3%;">
                                <div class="card">
                                    <div class="card-header"><a href="{{ route('products.show', $product->id) }}"><h5 style="text-align: center;">{{ $product->titulo }}</h5></a></div>

                                    <div class="card-body" style="text-align: center;"><img style="max-height: 200px; max-width: 350px;" src="{{ $product->miniatura }}" alt="{{ $product->miniatura }}"></div>

                                    <div class="card-footer" style="text-align: left;">
                                        <i class="fa-solid fa-star"></i> {{ $product->raiting ?? '?' }} de 5 <br>
                                        <i class="fa-solid fa-sack-dollar"></i> <i class="fa-solid fa-dollar-sign"></i> {{ $product->precio }} <br>
                                        <span style="font-weight: bold;"><i class="fa-solid fa-tags"></i> Descuento del: {{ $product->porcentajeDescuento}} <i class="fa-solid fa-percent"></i></span><br>

                                        <div style="text-align: center; padding-top: 3%;">
                                            @if (Auth::user()->role->name!="Cliente")
                                                <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary"><i class="fa-solid fa-pencil"></i> Editar producto</a><br>
                                            @else
                                                @if ($product->stock<=0)
                                                    <div class="btn btn-danger"><i class="fa-solid fa-cart-arrow-down"></i> No quedan unidades disponibles</div><br>
                                                @else
                                                    <form action="{{ route('shoppingCart.store') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="hidden" value="{{ $product->id }}" name="product_id" id="product_id">
                                                        <input type="hidden" value="{{ Auth::user()->id }}" name="user_id" id="user_id">
                                                        <button type="submit" class="btn btn-primary"><i class="fa-solid fa-cart-plus"></i> ¡Comprar ahora!</button><br>
                                                    </form>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
