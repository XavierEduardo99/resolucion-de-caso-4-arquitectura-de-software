@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4><i class="fa-solid fa-cash-register"></i> Registro de producto para la venta</h4>
                    <div style="padding-top: 1%;">
                        <a class="btn btn-success" href="{{  route('products.create')}}"><i class="fa-solid fa-plus"></i> Registrar nuevo producto</a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped table-hover" id="mainTable">
                        <thead>
                            <tr>
                                <th>Marca</th>
                                <th>Titulo</th>
                                <th>Precio</th>
                                <th>Descuento</th>
                                <th>Stock</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->marca }}</td>
                                <td>{{ $product->titulo }}</td>
                                <td>${{ $product->precio }}</td>
                                <td>{{ $product->porcentajeDescuento }}%</td>
                                <td>{{ $product->stock }} unidades</td>
                                <td>
                                    <div class="btn btn-sm btn-primary"><a style="color: #fafafa;" href="{{ route('products.show', $product->id) }}"><i class="fa-solid fa-eye"></i></a></div>
                                    <div class="btn btn-sm btn-info"><a style="color: #fafafa;" href="{{ route('products.edit', $product->id) }}""><i class="fa-solid fa-pencil"></i></a></div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        new DataTable('#mainTable');
    });
</script>

@endsection