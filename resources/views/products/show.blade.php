@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ $product->marca }} - {{ $product->titulo }}</h3>
                        <h5><i class="fa-solid fa-star"></i> {{ $product->raiting ?? '?' }} de 5</h5>
                        <h5><i class="fa-solid fa-sack-dollar"></i> <i class="fa-solid fa-dollar-sign"></i> {{ $product->precio }} <span style="font-weight: bold;"> - Tiene un descuento del {{ $product->porcentajeDescuento }} <i class="fa-solid fa-percent"></i></span></h5>
                        @if ($product->stock > 0)
                            <form action="{{ route('shoppingCart.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}">
                                <button type="submit" class="btn btn-primary"><i class="fa-solid fa-cart-shopping"></i> ¡Comprar ahora!</button><br>
                            </form>
                        @else
                            <span><span><i class="fa-solid fa-triangle-exclamation"></i>  No quedan más unidades disponibles...</span>
                        @endif
                    </div>

                    <div class="card-body">
                        <div style="text-align: center;"><img style="max-width: 600px;" src="{{ $product->miniatura }}" alt="{{ $product->miniatura }}"></div>
                        <div style="padding-top: 2%;">
                            <span style="font-weight: bold;">Descripción del producto:</span>
                            <textarea class="form-control" cols="30" rows="5" readonly>{{ $product->descripcion }}</textarea>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div style="text-align: center;">
                            Quedan: {{ $product->stock }} unidades disponibles
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection