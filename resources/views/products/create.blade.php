@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4><i class="fa-solid fa-cash-register"></i> Registro de producto para la venta</h4>
                </div>

                <div class="card-body">
                    <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="titulo">Titulo de la publicación:</label>
                                    <input type="text" class="form-control" id="titulo" name="titulo" maxlength="128" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="precio">Precio del producto:</label>
                                    <input type="number" class="form-control" id="precio" name="precio" min="1" max="999999" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="porcentajeDescuento">Porcentaje de descuento para el producto:</label>
                                    <input type="number" class="form-control" id="" name="porcentajeDescuento" min="0" max="100" maxlength="porcentajeDescuento" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="stock">Stock del producto:</label>
                                    <input type="number" class="form-control" id="stock" name="stock" min="1" max="999" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="marca">Marca del producto:</label>
                                    <input type="text" class="form-control" id="marca" name="marca" maxlength="128" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="miniatura">Enlace de imagen del producto:</label>
                                    <input type="text" class="form-control" id="miniatura" name="miniatura">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="descripcion">Descripción del producto:</label>
                                    <textarea name="descripcion" id="descripcion" cols="30" rows="5" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-top: 2%;">
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary"><i class="fa-solid fa-floppy-disk"></i> Guardar producto nuevo</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection