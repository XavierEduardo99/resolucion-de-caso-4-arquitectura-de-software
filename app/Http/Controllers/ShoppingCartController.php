<?php

namespace App\Http\Controllers;

use App\Models\ShoppingCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShoppingCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shoppingCarts=ShoppingCart::where('user_id', '=', Auth::user()->id)->get();
        $totalAPagar=0;

        foreach($shoppingCarts as $shoppingCart){
            $totalAPagar+=$shoppingCart->product->precio;
        }

        return view('shoppingCart.index', compact('shoppingCarts', 'totalAPagar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            ShoppingCart::create([
                'cantidadProducto' => 1,
                'user_id' => $request->user_id,
                'product_id' => $request->product_id,
            ]);
        }catch(Exception $e){
            dd($e);
        }

        return redirect()->route('home')->with('success', '¡Producto agregado al carrito exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShoppingCart $shoppingCart)
    {
        $shoppingCart->delete();
        return redirect()->route('shoppingCart.index')->with('success', '¡Producto removido exitosamente!');
    }

    public function showCheckout(){
        $shoppingCarts=ShoppingCart::where('user_id', '=', Auth::user()->id)->get();
        $totalAPagar=0;

        foreach($shoppingCarts as $shoppingCart){
            $totalAPagar+=$shoppingCart->product->precio;
        }

        return view('shoppingCart.showCheckout', compact('totalAPagar'));
    }
}
